var app = new Vue({
	el:"#app",
	data:{
		roles:['Developer', 'Climber', 'System Admin', 'Cyclist', 'Database Admin', 'DevOps Engineer', 'Hip-hop head','Project Manager', 'Audiophile', 'Hoosier'],
		role:0,
		project0:{
				name: "Rooster",
				usage: "Stock Analytics Webapp",
				status:"Production",
				statusText: "text-success",
				langFrame:["Python3", "MySQL", "Javascript", "Flask", "Vue"],
				snippet:`
to_return = []
if self.table_data[0][3] == True or self.table_data[0][3] == False:
    data = req.status_code
    if data == self.table_data[0][2]:
        code = True
    else:
        code = False
    to_return.append([self.table_data[0][0],self.table_data[0][1],code])
    # ^^this is used for checking for nager.date / similar
else:
    for call_var in self.table_data:
        data = req.json()
        if isinstance(data, str):
             data = json.loads(data)
        if call_var[3] == 'yank':
            if isinstance(call_var[2], list):
                for i in call_var[2]:
                    data = data[i]`
			},
		projects:[
			{
				name: "Linux Master Class",
				usage: "Linux Teaching Blog",
				status:"Production",
				statusText: "text-success",
				langFrame:["Yaml", "HTML", "Hugo"],
				snippet:`
---
title: "Home"
date: 2020-11-09T22:18:24-05:00
draft: true
---
# Linux Master Class
>The how and why of the Linux operating System
---

## Philosophy
>Why a Linux Doc site?

Linux Tutorials suffer from being too simple or too complicated.

## Where to start?

Go to the [Modules Tab](./modules/) to get started!
\`\`\`sh	
	user@linux:~$ echo 'Hello, Linux!'
	Hello, Linux!
\`\`\`
![Linux Word Cloud](/linuxWordCloud.png)
			`
			},
			{
				name: "Themisto.dev",
				usage: "Jupyter Notebooks as a service",
				status:"Development",
				statusText: "text-danger",
				langFrame:["Python3", "Docker", "Rust"],
				snippet:`
FROM continuumio/miniconda3

LABEL AUTHOR="jaredamcdon@gmail.com"

WORKDIR  /app

COPY environment.yml .
COPY static/images/full.svg .
COPY config/jupyterhub_config.py .

RUN conda env create -f environment.yml

SHELL ["conda", "run", "-n", "jupyter-env", "/bin/bash", "-c"]

RUN echo "verify jupyter install"
RUN python -c "import jupyterhub"

EXPOSE 4400

EXPOSE 6600
			`
			},
			{
				name: "Reverse Proxy",
				usage: "Dedicated functional reverse proxy as code",
				status:"Production",
				statusText: "text-success",
				langFrame:["Nix", "NginX", "NixOS"],
				snippet:`
  services.nginx = {
      enable = true;
      virtualHosts."www.jdogg.club jdogg.club lmc.jdogg.club" = {
        http2 = false;
        onlySSL = true;
        sslCertificateKey = "/etc/nginx/jdogg.club.key";
        sslCertificate = "/etc/nginx/jdogg.club.pem";
  
        locations = {
          "/" = {
            proxyPass = "http://192.168.3.25:80";
            };
        };
      };
  };
  services.avahi = {
  enable = true;
  nssmdns = true;
  openFirewall = true;
  publish = {
    enable = true;
    addresses = true;
    domain = true;
  };
  wideArea = true;
  hostName = "proxy";
  domainName = "local";
};
			`
			},
			{
				name: "SyncThing Instance",
				usage: "Automated server backups",
				status:"Production",
				statusText: "text-success",
				langFrame:["BASH","Systemd", "SyncThing", "OpenSuse"],
				snippet:`
[Unit]
Description=syncthing web server host

[Service]
User=syncthing
ExecStart=/usr/bin/syncthing
Restart=on-failure
StartLimitInterval=600

[Install]
WantedBy=multi-user.target
`
			},
			{
				name: "CSGO-KZ-Docker",
				usage: "Automated Counterstrike Servers",
				status:"Testing",
				statusText: "text-warning",
				langFrame:["BASH", "Docker"],
				snippet:`
version=$(sed 's/.*">//g' <<< $(curl https://bitbucket.org/kztimerglobalteam/kztimerglobal/downloads/?tab=downloads | grep "Full.zip" | head -1))

version=$(sed 's|</a>||g' <<< $version)
                
link="https://bitbucket.org/kztimerglobalteam/kztimerglobal/downloads/$version"
                
wget $link -O kzt.zip
unzip kzt.zip -d ./kzt
rm kzt.zip     
`
			},
			{
				name: "BASH One Time Pad",
				usage: "Insecure cryptographic algorithm in BASH",
				status:"Production",
				statusText: "text-success",
				langFrame:["BASH"],
				snippet:`
function decrypt_fn {
    len=\${#cipher}
                
    keyNumeric=()
    cipherNumeric=()
    for ((i = 0 ; i < $len ; i++))
    do
        current=\${cipher:$i:1}
        cipherNumeric+=(\$\{alphabet[$current]\})
        current=\${key:$i:1}
        keyNumeric+=(\${alphabet[$current]})
    done
                
    decrypt=""
    for ((i = 0; i < $len ; i++))
    do
        decryptNum=$(expr \$\{cipherNumeric[$i]} - \$\{keyNumeric[$i]\})
        if [ $decryptNum -lt 0 ]
        then
            decrypt+=\${revAlphabet[$(expr $decryptNum + 27)]}
        else
            decrypt+=\${revAlphabet[$(expr $decryptNum % 27)]}
        fi
    done
}
`
			}
		],
		carousel:[
			{
				title:"Introduction",
				secondary:"I'm a climber, developer, and overall technologist with a passion for open source."
			},
			{
				title:"Gitlab",
				secondary:"It's where I host most my contributions, sorted by language"
			},
			{
				title:"CodePen",
				secondary:"My frontend playground, check out my demos"
			},
			{
				title:"Github",
				secondary:"This is where I host my bigger open-source projects"
			},
			{
				title:"YouTube",
				secondary:"YouTube is where I post my Linux videos and dev logs"
			},
			{
				title:"LinkedIn",
				secondary: "Get a look at my experience and skills"
			}
		]
	},
	methods:{
		
	}
})

function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

var rolodex = async function(){
	await delay(1500);
	if (app._data.role < app._data.roles.length - 1){
		app._data.role++;
	}
	else{
		app._data.role = 0;
	}
	rolodex();
}

window.onload = rolodex();