Vue.component('person',{
  props:['name', 'year','starter','idx'],
  template:'<h2 v-on:click="add">{{name}} : {{starter}}</h2>',
  methods:{
    add:function(){
      this.starter++;
      app._data.movies[this.idx].start++;
      
    }
  }
})

var app = new Vue({
  el:"#app",
  methods:{
    checkYear:function(){
      for (var i = 0; i < app._data.movies.length; i++){
        if(app._data.movies[i].start ==  app._data.movies[i].year){
          app._data.passFail[i] = "pass";
        }
        else{
          app._data.passFail[i] = "fail";
        }
      }
      if(app._data.passFail.includes("fail")){
        this.getCaged = "You got caged, try again Mack"
      }
      else if(app._data.passFail.includes("null")){
        this.getCaged = "You got caged, try again Mack"
      }
      else{
        this.getCaged = "Click the button for loot"
        this.theHider = "itBeVisible"
      }
    }
  },
  data:{
    theHider:"itBeHidden",
    movies:[
      {
        title: "Face/Off", year: 1997, start:1997, idx:0
      },
      {
        title: "National Treasure", year: 2004, start:1997, idx:1
      }, 
      {
        title: "Lord of War", year: 2005, start:1997, idx:2
      }, 
      {
        title: "Ghost Rider", year: 2007, start:1997, idx:3
      }, 
      {
        title: "National Treasure: Book of Secrets", year: 2007, start:1997, idx:4
      }, 
      {
        title: "Next", year: 2007, start:1997, idx:5
      }, 
      {
        title: "Bangkok Dangerous", year: 2008, start:1997, idx:6
      }, 
      {
        title: "Knowing", year: 2009, start:1997, idx:7
      }, 
      {
        title: "Kick-Ass", year: 2010, start:1997, idx:8
      }, 
      {
        title: "Drive Angry", year: 2011, start:1997, idx:9
      }, 
      {
        title: "Season of the Witch", year: 2011, start:1997, idx:10
      }
    ],
    passFail:['null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null'],
    getCaged:""
}
})