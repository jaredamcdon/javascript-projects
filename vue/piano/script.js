app = new Vue({
  el:"#app",
  data:{
    keys:[
      {name:'C3', color:'white', location:'./3/C3.wav', kb:'Q'},
      {name:'C#3', color:'black', location:'./3/Csharp3.wav',kb:'2'},
      {name:'D3', color:'white', location:'./3/D3.wav',kb:'W'},
      {name:'D#3', color:'black', location:'./3/Dsharp3.wav',kb:'3'},
      {name:'E3', color:'white', location:'./3/E3.wav',kb:'E'},
      {name:'F3', color:'white', location:'./3/F3.wav',kb:'R'},
      {name:'F#3', color:'black', location:'./3/Fsharp3.wav',kb:'5'},
      {name:'G3', color:'white', location:'./3/G3.wav',kb:'T'},
      {name:'G#3', color:'black', location:'./3/Gsharp3.wav',kb:'6'},
      {name:'A4', color:'white', location:'./4/A4.wav',kb:'Y'},
      {name:'A#4', color:'black', location:'./4/Asharp4.wav',kb:'7'},
      {name:'B4', color:'white', location:'./4/B4.wav',kb:'U'},
      {name:'C4', color:'white', location:'./4/C4.wav',kb:'I / Z'},
      {name:'C#4', color:'black', location:'./4/Csharp4.wav',kb:'9 / S'},
      {name:'D4', color:'white', location:'./4/D4.wav',kb:'O / X'},
      {name:'D#4', color:'black', location:'./4/Dsharp4.wav',kb:'0 / D'},
      {name:'E4', color:'white', location:'./4/E4.wav',kb: 'C'},
      {name:'F4', color:'white', location:'./4/F4.wav',kb:'V'},
      {name:'F#4', color:'black', location:'./4/Fsharp4.wav',kb:'G'},
      {name:'G5', color:'white', location:'./4/G4.wav',kb:'B'},
      {name:'G#5', color:'black', location:'./4/Gsharp4.wav',kb:'H'},
      {name:'A5', color:'white', location:'./5/A5.wav',kb:'N'},
      {name:'A#5', color:'black', location:'./5/Asharp5.wav',kb:'J'},
      {name:'B5', color:'white', location:'./5/B5.wav',kb:'M'},
      {name:'C5', color:'white', location:'./5/C5.wav',kb:','},
    ]
  },
  methods:{
      playSound:function(audioClip){
          var keySound = new Audio(audioClip, 100, false)
          keySound.play();
          console.log(audioClip);
      }
  }
})

var counter = 0;
counterEle = document.getElementById("counter");
counterEle.innerHTML = counter;

function keyUpNote(event){
    console.log(event);
    var keyPressed = event.which || event.keyCode;
    console.log(keyPressed);
    if(keyPressed == '113'){
       var keySound = new Audio('./3/C3.wav', 20, false)
       }
    else if(keyPressed == '119'){
       var keySound = new Audio('./3/D3.wav', 50, false)
       }
    else if(keyPressed == '101'){
       var keySound = new Audio('./3/E3.wav', 50, false)
       }
    else if(keyPressed == '114'){
       var keySound = new Audio('./3/F3.wav', 50, false)
       }
    else if(keyPressed == '116'){
       var keySound = new Audio('./3/G3.wav', 50, false)
       }
    else if(keyPressed == '121'){
       var keySound = new Audio('./4/A4.wav', 50, false)
       }
    else if(keyPressed == '117'){
       var keySound = new Audio('./4/B4.wav', 50, false)
       }
    else if(keyPressed == '105'){
       var keySound = new Audio('./4/C4.wav', 50, false)
       }
    else if(keyPressed == '111'){
       var keySound = new Audio('./4/D4.wav', 50, false)
       }
    else if(keyPressed == '112'){
       var keySound = new Audio('./4/E4.wav', 50, false)
       }
    else if(keyPressed == '50'){
       var keySound = new Audio('./3/Csharp3.wav', 50, false)
       }
    else if(keyPressed == '51'){
       var keySound = new Audio('./3/Dsharp3.wav', 50, false)
       }
    else if(keyPressed == '53'){
       var keySound = new Audio('./3/Fsharp3.wav', 50, false)
       }
    else if(keyPressed == '54'){
       var keySound = new Audio('./3/Gsharp3.wav', 50, false)
       }
    else if(keyPressed == '55'){
       var keySound = new Audio('./4/Asharp4.wav', 50, false)
       }
    else if(keyPressed == '57'){
       var keySound = new Audio('./4/Csharp4.wav', 50, false)
       }
    else if(keyPressed == '48'){
       var keySound = new Audio('./4/Dsharp4.wav', 50, false)
       }
    else if(keyPressed == '122'){
       var keySound = new Audio('./4/C4.wav', 20, false)
       }
    else if(keyPressed == '120'){
       var keySound = new Audio('./4/D4.wav', 50, false)
       }
    else if(keyPressed == '99'){
       var keySound = new Audio('./4/E4.wav', 50, false)
       }
    else if(keyPressed == '118'){
       var keySound = new Audio('./4/F4.wav', 50, false)
       }
    else if(keyPressed == '98'){
       var keySound = new Audio('./4/G4.wav', 50, false)
       }
    else if(keyPressed == '110'){
       var keySound = new Audio('./5/A5.wav', 50, false)
       }
    else if(keyPressed == '109'){
       var keySound = new Audio('./5/B5.wav', 50, false)
       }
    else if(keyPressed == '44'){
       var keySound = new Audio('./5/C5.wav', 50, false)
       }
    //
    else if(keyPressed == '115'){
       var keySound = new Audio('./4/Csharp4.wav', 50, false)
       }
    else if(keyPressed == '100'){
       var keySound = new Audio('./4/Dsharp4.wav', 50, false)
       }
    else if(keyPressed == '103'){
       var keySound = new Audio('./4/Fsharp4.wav', 50, false)
       }
    else if(keyPressed == '104'){
       var keySound = new Audio('./4/Gsharp4.wav', 50, false)
       }
    else if(keyPressed == '106'){
       var keySound = new Audio('./5/Asharp5.wav', 50, false)
       }
    if(keySound){
       keySound.play();
        counter++;
        counterEle.innerHTML = counter;
       }
}