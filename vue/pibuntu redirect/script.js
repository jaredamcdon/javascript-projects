var app = new Vue({
	el:"#app",
	data:{
		servers: serverData,
		sites: siteData
	},
	methods:{
		showHide(passedServer){
			for(var i = 0; i < app._data.sites.length; i++){
				if(passedServer == app._data.sites[i].server){
					app._data.sites[i].hidden = !app._data.sites[i].hidden;
				}
			}
		}
	}
})
