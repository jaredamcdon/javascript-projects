var serverData = [
    {
        name: "silverServer",
        checked: true
    },
    {
        name: "pibuntu",
        checked: true
    },
    {
        name: "debianDocker",
        checked: true
    },
    {
        name: "optiflex",
        checked: true
    },
    {
        name: "other",
        checked: true
    }
]

var siteData = [
    {
        name: "Plex",
        addresses: ["http://192.168.3.15:32400", "http://silverserver.local:32400", "http://plex.jdogg.club", "https://app.plex.tv"],
        server: "silverServer",
        web: true,
        info: "Plex location",
        hidden: false,
        imageTag: "svg/play-btn.svg"
    },
    {
        name: "silverServer Apache2",
        addresses:["http://192.168.3.15/", "http://silverserver.local"],
        server: "silverServer",
        web: true,
        info: "Default Apache2 page @ silverServer",
        hidden: false,
        imageTag: "svg/globe.svg"
    },
    {
        name: "jdogg.club",
        addresses: ["http://192.168.3.25/", "http://debiandocker.local", "https://192.168.3.25/", "https://jdogg.club"],
        server:"debianDocker",
        web: true,
        info: "Local site connected to jdogg.club domain",
        hidden: false,
        imageTag: "svg/globe.svg"
    },
    {
        name: "pibuntu web direct",
        addresses: ["http://192.168.3.55/", "http://pibuntu.local"],
        web: true,
        server:"pibuntu",
        info: "Redirect page for local services",
        hidden: false,
        imageTag: "svg/globe.svg"
    },
    {
        name: "jupyter notebook",
        addresses: ["http://192.168.3.25:8888", "http://debiandocker.local:8888"],
        web: true,
        server:"debianDocker",
        info: "Python notebook",
        hidden: false,
        imageTag: "svg/journal-code.svg"
    },
    {
        name: "PhpMyAdmin",
        addresses: ["http://192.168.3.45/phpmyadmin", "https://192.168.3.45/phpmyadmin", "https://optiflex.local/phpmyadmin"],
        web: true,
        server:"optiflex",
        info: "MySQL admin page; LAMP stack based",
        hidden: false,
        imageTag: "svg/toggles.svg"
    },
    {
        name: "MariaDB Instance",
        addresses: ["192.168.3.45:3306", "optiflex.local:3306"],
        web: false,
        server:"optiflex",
        info: "Database for testing, remote account enabled",
        hidden: false,
        imageTag: "svg/minecart-loaded.svg"
    },
    {
        name: "Router webpage",
        addresses: ["http://192.168.3.1"],
        web: true,
        server:"other",
        info: "Web management page",
        hidden: false,
        imageTag: "svg/wifi.svg"
    },
    {
        name: "Switch webpage",
        addresses: ["http://192.168.3.2"],
        web: true,
        server: "other",
        info: "Management and configuration",
        hidden: false,
        imageTag: "svg/hdd.svg"
    },
    {
        name: "Printer management",
        addresses: ["http://192.168.3.99"],
        web: true,
        server: "other",
        info: "Configuration and services",
        hidden: false,
        imageTag: "svg/printer.svg"
    },
    {
	name: "draw.io",
        addresses: ["http://192.168.3.25:8080", "debiandocker.local:8080"],
        web: true,
        server: "debianDocker",
        info: "UML tooling",
        hidden: false,
        imageTag: "svg/journal-code.svg"
    },
    {
	name: "mongodb",
        addresses: ["http://192.168.3.25:27017", "debiandocker.local:27017","jdogg.club:27017","75.114.200.67:27017"],
        web: false,
        server: "debianDocker",
        info: "mongodb database instance",
        hidden: false,
        imageTag: "svg/minecart-loaded.svg"
    },
    {
	name: "mongo express",
        addresses: ["http://192.168.3.25:8081", "debiandocker.local:8081"],
        web: true,
        server: "debianDocker",
        info: "mongodb management page",
        hidden: false,
        imageTag: "svg/toggles.svg"
    },
    {
	name: "Metabase",
        addresses: ["192.168.3.25:3000", "debiandocker.local:3000"],
        web: true,
        server: "debianDocker",
        info: "data visualization/business intelligence tool",
        hidden: false,
        imageTag: "svg/graph.svg"
    }
]
