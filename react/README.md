## React apps

### Commands
- create new
  - `npx create-react-app {app-name}`

- run locally
  - `npm start`

- export to static build/ dir
  - `npm run build`

- run static site
  - make sure you install serve
    - `sudo npm install -g serve`
  - `serve build`
