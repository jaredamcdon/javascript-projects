import React from "react"
import styles from "./TodoItem.module.css"

function TodoItem(props) {
    const completedStyle = {
        fontStyle: "italic",
        color: "#595959",
        opacity: 0.4,
        textDecoration: "line-through"
    }
    //destructuring to allow local naming of vars (props.todo.id => id)
    const { completed, id, title } = props.todo;
    return (
        <li className={styles.item}>
            <input
                type="checkbox"
                className={styles.checkbox}
                checked={completed}
                onChange={() => props.handleChangeProps(id)}
            />
            <button
                onClick={() => props.deleteTodoProps(id)}
            >
                Delete
            </button>
            <span
            style={
                completed ? completedStyle : null
            }
            >
                {title}
            </span>
        </li>
    )
}

export default TodoItem

/*
this also works but different ofc

import React from "react"

class TodoItem extends React.Component {
    render () {
        return <li>{this.props.todo.title}</li>
    }
}

export default TodoItem
*/