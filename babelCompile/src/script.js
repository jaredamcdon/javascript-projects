function formatName(user) {
  return `${user.firstName} ${user.lastName}`;
}

const user = {
  firstName: 'Jared',
  lastName: 'McDonald'
};
const element = /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h1", null, "Hello, ", formatName(user)), /*#__PURE__*/React.createElement("p", null, "We dont see many ", user.firstName, "'s around these parts"));
ReactDOM.render(element, document.getElementById('root'));
