//var firstEle = document.body.querySelector("#first");
var firstEle = document.forms["form"]["fname"]; 
var lastEle = document.forms["form"]["lname"]; 
var userEle = document.forms["form"]["uname"]; 
var passEle = document.forms["form"]["pword"]; 
var mailEle = document.forms["form"]["email"]; 

var firstErrEle = document.body.querySelector("#firstErr");
var lastErrEle = document.body.querySelector("#lastErr");
var userErrEle = document.body.querySelector("#userErr");
var passErrEle = document.body.querySelector("#passErr");
var mailErrEle = document.body.querySelector("#mailErr");

var mailCheck = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

function signup(){
	if (firstEle.value.length < 3){
		firstErrEle.innerHTML = 'First name must be 3 or more characters';
	}
	else{
		firstErrEle.innerHTML = '';
	}
	if (lastEle.value.length < 3){
		lastErrEle.innerHTML = 'Last name must be 3 or more characters';
	}
	else{
		lastErrEle.innerHTML = '';
	}
	if (userEle.value.length < 5){
		userErrEle.innerHTML = 'Usernames must be at least 5 characters';
	}
	else if(/[^a-zA-Z0-9_-]/.test(userErrEle.value)){
		userErrEle.innerHTML = 'Only a-z, A-Z, 0-9, - and _ allowed in usernames';
	}
	else{
		userErrEle.innerHTML = '';
	}
	if (passEle.value.length < 6){
		passErrEle.innerHTML = 'Passwords must be at least 6 characters';
	}
	else if (!/[a-z]/.test(passEle.value) || !/[A-Z]/.test(passEle.value) || ![0-9].test(passEle.value)){
		passErrEle.innerHTML = 'Passwords require one each of a-z, A-Z, and 0-9';
	}
	else{
		passErrEle.innerHTML = '';
	}
	if (mailCheck.test(mailEle.value)){
		mailErrEle.innerHTML = 'Email address is invalid';
	}
	else{
		mailErrEle.innerHTML = '';
	}
}