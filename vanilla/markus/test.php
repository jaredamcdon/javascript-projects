<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="Testing site for PHP"/>
    <meta name="author" content="Mark McDonald"/>
    <meta name="keywords" content="php, testing"/>
    <meta name="viewport" content="width=device-width, initial=scale-1.0"/>
    <meta name="HandheldFriendly" content="true"/>
    <title>AC Management</title>
    <link rel="stylesheet" type="text/css" href="style.css"/>
  </head>

<body>

<div id="container">
    <div id="header">
        <h1>Aircraft Management</h1>

    </div>
        <div id="nav">
          <div class="nav a">
            <a href="index.php">Home</a>
            <a href="expense.php">Expense Entry</a>
            <a href="records.php">Records</a>
            <a href="setupuser.php">Manage Users</a>
          </div>

        </div>

    <div id="main">
        <h4 style="text-align:center">Create a New User</h4>

        <table class="signup" border="0" cellpadding="2"
                cellspacing="5" bgcolor="#eeeeee">
        <th colspan="2" align="center">Signup Form</th>
        <form method="post" action="setupuser.php" onSubmit="return validate(this)" name="form">
          <tr>
            <td>First Name</td>
            <td>
              <input type="text" id = "first" maxlength="32" name="fname" autofocus="autofocus"/>
            </td>
            <td>
              <p id = 'firstErr'></p>
            </td>
          </tr>
          <tr>
            <td>Last Name</td>
            <td>
              <input type="text" id = "last" maxlength="32" name="lname" required='required'/>
            </td>
            <td>
              <p id = 'lastErr'></p>
            </td>
          </tr>
          <tr>
            <td>Username</td>
            <td>
              <input type="text" id = "user" maxlength="32" name="uname" required='required'/>
            </td>
            <td>
              <p id = 'userErr'></p>
            </td>
          </tr>
          <tr>
            <td>Password</td>
            <td>
              <input type="text" id = "pass" maxlength="32" name="pword" required='required'/>
            </td>
            <td>
              <p id = 'passErr'></p>
            </td>
          </tr>
          <tr>
            <td>Email</td>
            <td>
              <input type="text" id = "mail" maxlength="32" name="email" required='required'/>
            </td>
            <td>
              <p id = 'mailErr'></p>
            </td>
          </tr>
          <tr>
            <td colspan="2" align="center"><input type="submit" value="Signup" onclick = "signup()"></td>
          </tr>
        </form>
      </table>
      <script src="./script.js"></script>
        <?php
        require_once 'login.php';
        $link = new mysqli($hn, $un, $pw, $db);
        if ($link->connect_error) die("Oh sorry! couldn't connect you to the database.");


        if (!empty($_POST['fname']) &&
            !empty($_POST['lname']) &&
            !empty($_POST['uname']) &&
            !empty($_POST['pword']) &&
            !empty($_POST['email']))
        {
          $stmt = $link->prepare('INSERT into users (firstName, lastName, user_name, pass_word, e_mail) VALUES (?,?,?,?,?)');

          $firstname = $_POST['fname'];
          $lastname = $_POST['lname'];
          $username = $_POST['uname'];
          $pword = $_POST['pword'];
          $email = $_POST['email'];
          $hash = password_hash($pword, PASSWORD_DEFAULT);

          $stmt->bind_param('sssss', $firstname, $lastname, $username, $hash, $email);
          $stmt->execute();
          $link->close();
          echo 'User Created <br>';
        }
          else {
          echo $stmt->error;
        }

        ?>


    </div>

    <div id="footer">
      Copyright &copy; 2020 Mark McDonald
    </div>

  </div>
</body>

</html>

